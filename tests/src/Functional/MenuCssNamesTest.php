<?php

namespace Drupal\Tests\menu_css_names\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the menu_css_names module.
 *
 * @group menu_css_names
 */
class MenuCssNamesTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'toolbar',
    'menu_css_names',
  ];

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A simple user.
   *
   * @var \Drupal\user\UserInterface
   */
  private $user;

  /**
   * Perform initial setup tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'access toolbar',
      'access content overview',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the Content admin toolbar css class is present.
   */
  public function testClassExists() {
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Check if the "Content" admin toolbar css class is present.
    $this->assertSession()->elementExists('css', 'li.content');
  }

}
