<?php

namespace Drupal\menu_css_names\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Admin config form for Menu CSS Names module.
 */
class MenuCssNamesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'menu_css_names.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'menu_css_names_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['local_actions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include classes for Local actions'),
      '#default_value' => $this->config('menu_css_names.settings')->get('local_actions'),
    ];

    $form['tasks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include classes for Local Tasks'),
      '#default_value' => $this->config('menu_css_names.settings')->get('tasks'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('menu_css_names.settings');

    $config->set('local_actions', $form_state->getValue('local_actions'));
    $config->set('tasks', $form_state->getValue('tasks'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
