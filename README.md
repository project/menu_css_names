# Menu CSS Names

## Table of Contents

- Introduction
- Features
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

The Menu CSS Names module adds CSS class names to Drupal menus.

* For a full description of the module, visit the
  [project page](https://www.drupal.org/project/menu_css_names).

* To submit bug reports and feature suggestions, or to track changes visit the
  [issue page](https://drupal.org/project/issues/menu_css_names)

## Features

The module takes the title of each Drupal menu item and adds it as a css class name to the
menu's `<li>` element. Any character from this title that is not an alphanumeric character,
dash, or underscore is converted to a dash; all letters will be converted to lowercase.

Using these class names, css can be used to style each menu item separately as needed or
CSS sprite techniques can be used. For a menu item whose title is "Product Information",
a typical CSS rule would look like this:

```
  ul.menu li.product-information { font-style: bold; }
```

## Requirements

This module requires no modules outside of Drupal core.

## Installation

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/node/1897420) for further information.

## Configuration

There are admin settings for local actions and local tasks, for other menus it
starts doing its work once the module is enabled. All caches are automatically
cleared at this time also.

## Maintainers
Current maintainers:
* Rian Callahan - [rc_100](https://www.drupal.org/u/rc_100)
* Neha - [nehajyoti](https://www.drupal.org/u/nehajyoti)
* Aaron - [aaron.ferris](https://www.drupal.org/u/aaronferris)

This project is sponsored by:
* [Ashday Interactive Systems](https://www.drupal.org/ashday-interactive-systems)
* [Zoocha](https://www.drupal.org/zoocha)
